# cd_coverage
A Rust command line tool to interpret export information from `llvm-cov`
and extrapolate viable condition/decision coverage information.

# Usage:
1. Create JSON file from clang
    * Call clang with instrumentation: 
    `clang++ -fprofile-instr-generate=<NAME>.raw -fcoverage-mapping <FILES> -o <OUT>`
    * Run instrumented binary:
    `<OUT> <ARGS>`
    * Create profdata:
    `llvm-profdata merge -o <NAME>.prof <NAME>.raw`
    * Get export file:
    `llvm-cov export -Xdemangler=llvm-cxxfilt -instr-profile=<NAME>.prof <FILES> > <EXPORT>`
2. Convert to CD coverage format
    * `llvm-cdc -i=<EXPORT> -o=<CD-COVERAGE>`
    
    
# Notes:
* Currently is nightly only
  * Depends on Nonlexical-lifetimes