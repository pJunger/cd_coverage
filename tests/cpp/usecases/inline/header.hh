#pragma once

#include <cstdint>
#include <limits>

inline int make_even(int argument) {
  if (argument % 2) {
    return argument * 2;
  } else {
    return argument;
  }
}

namespace processor {
  extern int process(int argument);
}

template<std::size_t N>
int transform() {
  if (N > std::numeric_limits<int>::max()) {
    return 0;
  } else {
    return static_cast<int>(N);
  }
}