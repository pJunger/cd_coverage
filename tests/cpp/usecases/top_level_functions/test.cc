#include <optional>

int one() {
  return 1;
}

int two() {
  return std::make_optional(2).value();
}


int main(int argc, char const *argv[]) {
  return one() + two();
}
