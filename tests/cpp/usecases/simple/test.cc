#include "header.hh"

#include <string>
#include <stdexcept>

using namespace std::literals;

template<int N>
class TestClass {
 public:
  // Are default constructors visible in results?
  TestClass() = default;
  TestClass(int i) : val{i} {
    if (i > (2 * N)) {
      throw std::out_of_range("Was out of range");
    }
  }
  ~TestClass() = default;
  TestClass(TestClass &&) = default;
  TestClass(TestClass const &) = default;

  // variant results
  auto ret_int() -> int {
    if (N > val) {
      return N / 2;
    } else {
      return N;
    }
  }

  // invariant, but interesting for cdc
  auto ret_str(int i0, int i1, int i2) -> std::string {
    if ((i0 > 10) && ((i1 == i2) || (i0 == i2))) {
      return "true"s;
    } else {
      return "false"s;
    }
  }

  // interesting only for function coverage
  auto ret_int2() -> int {
    MACRO_IF(this->val);
  }

 private:
  int val = 0;
};

int main(int argc, char const *argv[]) {
  TestClass<11> tc1{};
  TestClass<10> tc2{10};
  try {
    TestClass<1> tc3{1000};
  } catch (std::out_of_range const &) {

  }

  auto fail0 = tc1.ret_str(10, 1, 1);
  auto fail1 = tc1.ret_str(11, 1, 5);
  auto sucess0 = tc1.ret_str(11, 1, 1);
  auto sucess1 = tc1.ret_str(11, 1, 11);
  return tc1.ret_int2() + tc1.ret_int() + tc2.ret_int();
}
