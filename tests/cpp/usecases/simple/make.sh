#!/usr/bin/env bash

SOURCES=test.cc
BINARY=test.out
RAWPROF=test.profraw
PROFDATA=test.profdata
JSON=export.json

CLANG="clang++ -std=c++14"
LLVMPROFDATA=llvm-profdata
LLVMCOV=llvm-cov
DEMANGLER=llvm-cxxfilt

# Clean previous results
rm -rf *.json
rm -rf *.profdata
rm -rf *.profraw
rm -rf *.out

# Compile with instrumentation for llvm-cov
$CLANG -fprofile-instr-generate=$RAWPROF -fcoverage-mapping $SOURCES -o $BINARY
# Collect profiling data
./$BINARY
# Convert raw data
$LLVMPROFDATA merge -o $PROFDATA $RAWPROF
# Get export json file
$LLVMCOV export -expensive-combines -Xdemangler=$DEMANGLER -verify-region-info -instr-profile=$PROFDATA $BINARY > $JSON

# Remove machine-dependent absolute paths
sed 's;/\([A-Za-z0-9_-]*/\)*tests/;./tests/;g' $JSON > $JSON.bak && mv $JSON.bak $JSON