struct IterSomething {
    IterSomething() {
        array[5] = 3;
    }

    int* begin() {
        return array;
    }

    int* end() {
        return &(array[9]);
    }
    
    int array[10];
};


int main() {
    for (int const & i: IterSomething{})
        if (i == 3) break;

    return 5;
}