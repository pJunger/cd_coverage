extern crate cd_coverage;
extern crate env_logger;
extern crate log;

use std::path::PathBuf;

use self::env_logger::{Builder, Target};
use cd_coverage::error;

// Todo: Add assertions here! Currently, all is well if it returns Ok

fn get_args(input: &str, output: &str) -> cd_coverage::arguments::Arguments {
    cd_coverage::arguments::Arguments {
        input: PathBuf::from(input),
        output: PathBuf::from(output),
        log_filter: None,
    }
}

fn try_init_logger(log_level: log::LevelFilter) -> Result<(), log::SetLoggerError> {
    Builder::new()
        .is_test(true)
        .filter_level(log_level)
        .target(Target::Stdout)
        .try_init()
}

fn run_test(input: &str, output: &str, log_level: log::LevelFilter) -> error::Result<()> {
    let _ = try_init_logger(log_level);
    let args = get_args(input, output);
    cd_coverage::create_coverage_results(args)?;
    Ok(())
}

#[test]
fn test_conditional_operator() -> error::Result<()> {
    run_test(
        "tests/cpp/decisions/conditional_operator/test.json",
        "tests/cpp/decisions/conditional_operator/cdc.json",
        log::LevelFilter::Trace,
    )
}

//#[test]
//fn test_do_while() -> error::Result<()> {
//    run_test(
//        "tests/cpp/decisions/do_while/test.json",
//        "tests/cpp/decisions/do_while/cdc.json",
//        log::LevelFilter::Trace,
//    )
//}

#[test]
fn test_for() -> error::Result<()> {
    run_test(
        "tests/cpp/decisions/for/test.json",
        "tests/cpp/decisions/for/cdc.json",
        log::LevelFilter::Warn,
    )
}

#[test]
fn test_foreach() -> error::Result<()> {
    run_test(
        "tests/cpp/decisions/foreach/test.json",
        "tests/cpp/decisions/foreach/cdc.json",
        log::LevelFilter::Trace,
    )
}

#[test]
fn test_if() -> error::Result<()> {
    run_test(
        "tests/cpp/decisions/if/test.json",
        "tests/cpp/decisions/if/cdc.json",
        log::LevelFilter::Warn,
    )
}

#[test]
fn test_if_block() -> error::Result<()> {
    run_test(
        "tests/cpp/decisions/if_block/test.json",
        "tests/cpp/decisions/if_block/cdc.json",
        log::LevelFilter::Trace,
    )
}

#[test]
fn test_switch() -> error::Result<()> {
    run_test(
        "tests/cpp/decisions/switch/test.json",
        "tests/cpp/decisions/switch/cdc.json",
        log::LevelFilter::Trace,
    )
}

#[test]
fn test_while() -> error::Result<()> {
    run_test(
        "tests/cpp/decisions/while/test.json",
        "tests/cpp/decisions/while/cdc.json",
        log::LevelFilter::Trace,
    )
}
