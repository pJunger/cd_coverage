#[macro_use]
extern crate log;
extern crate cd_coverage;
extern crate env_logger;

use std::path::PathBuf;

use self::env_logger::{Builder, Target};
use cd_coverage::error;

// Todo: Add assertions here! Currently, all is well if it returns Ok

fn get_args(input: &str, output: &str) -> cd_coverage::arguments::Arguments {
    cd_coverage::arguments::Arguments {
        input: PathBuf::from(input),
        output: PathBuf::from(output),
        log_filter: None,
    }
}

fn try_init_logger() -> Result<(), log::SetLoggerError> {
    Builder::new()
        .is_test(true)
        .filter_level(log::LevelFilter::Debug)
        .target(Target::Stdout)
        .try_init()
}

//#[test]
//fn test_0000_top_level_functions() -> error::Result<()> {
//    let _ = try_init_logger();
//    info!("Running test_0000_top_level_functions");
//    let args = get_args(
//        "tests/c/0000_top_level_functions/export.json",
//        "tests/c/0000_top_level_functions/cdc.json",
//    );
//    let model = cd_coverage::create_coverage_results(args)?;
//    debug!("The model is: {:?}", model);
//    Ok(())
//}

#[test]
fn test_0001_simple() -> error::Result<()> {
    let _ = try_init_logger();
    info!("Running test_0001_simple");

    let args = get_args(
        "tests/c/0001_simple/export.json",
        "tests/c/0001_simple/cdc.json",
    );

    let model = cd_coverage::create_coverage_results(args)?;
    debug!("The model is: {:?}", model);

    Ok(())
}

//#[test]
//fn test_0100_static_inline() -> error::Result<()> {
//    let _ = try_init_logger();
//    info!("Running test_0100_static_inline!");
//    let args = get_args(
//        "tests/c/0100_static_inline/export.json",
//        "tests/c/0100_static_inline/cdc.json",
//    );
//    let model = cd_coverage::create_coverage_results(args)?;
//    debug!("The model is: {:?}", model);
//    Ok(())
//}
//
//#[test]
//fn test_0101_inline() -> error::Result<()> {
//    let _ = try_init_logger();
//    info!("Running test_0101_inline");
//    let args = get_args(
//        "tests/c/0101_inline/export.json",
//        "tests/c/0101_inline/cdc.json",
//    );
//    let model = cd_coverage::create_coverage_results(args)?;
//    debug!("The model is: {:?}", model);
//    Ok(())
//}
