
def make(sources, binary_name)
    clang='clang'
    llvm_prof_data='llvm-profdata'
    llvm_cov='llvm-cov'
    demangler='llvm-cxxfilt'
    sed='sed'
    suffix = Gem.win_platform? ? '.exe' : '.out'

    binary = binary_name + suffix
    raw_prof = "#{binary_name}.profraw"
    prof_data = "#{binary_name}.profdata"
    json = "#{binary_name}.json"

    # Clean previous results
    Dir.each_child(Dir.pwd)
    .select {|f| f =~ /\.*(json|profdata|profraw|out|exe)/ }
    .each { |file| File.delete(file)}

    # Compile with instrumentation for llvm-cov
    system("#{clang} -fprofile-instr-generate=#{raw_prof} -fcoverage-mapping #{sources.join(' ')} -o #{binary}")
    # Collect profiling data
    system("./#{binary}")
    # Convert raw data
    system("#{llvm_prof_data} merge -o #{prof_data} #{raw_prof}")
    # Get export json file
    system("#{llvm_cov} export -Xdemangler=#{demangler} -instr-profile=#{prof_data} #{binary} > #{json}")

    # Remove machine-dependent absolute paths
    system("#{sed} 's;/\([A-Za-z0-9_-]*/\)*tests/;./tests/;g' #{json} > #{json}.bak && mv #{json}.bak #{json}")
end