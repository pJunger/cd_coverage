#pragma once

inline int make_even(int argument) {
  if (argument % 2) {
    return argument * 2;
  } else {
    return argument;
  }
}

extern int process(int argument);