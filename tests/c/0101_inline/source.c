#include "header.h"

extern int make_even(int argument);

int process(int argument) {
  return make_even(argument + 3);
}
