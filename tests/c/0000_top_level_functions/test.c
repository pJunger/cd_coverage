int one() {
  return 1;
}

int two() {
  return one() - 1;
}


int main(int argc, char const *argv[]) {
  return one() + two();
}
