#include "header.h"

#ifdef __cplusplus
extern "C" {
#endif

int main(void) {
  return make_even(5) + process(3);
}

#ifdef __cplusplus
}
#endif

