#include "header.h"


#ifdef __cplusplus
extern "C" {
#endif

int process(int argument) {
  return make_even(argument + 3);
}

#ifdef __cplusplus
}
#endif
