#!/usr/local/bin/ruby -w

sources=['test.c']
binary='test.out'
raw_prof='test.profraw'
prof_data='test.profdata'
json='export.json'
sed='sed'

clang='clang'
llvm_prof_data='llvm-profdata'
llvm_cov='llvm-cov'
demangler='llvm-cxxfilt'

# Clean previous results
Dir.each_child('.')
     .select {|f| f =~ /\.*(json|profdata|profraw|out|exe)/ }
	 .each { |file| File.delete(file)}

# Compile with instrumentation for llvm-cov
`#{clang} -fprofile-instr-generate=#{raw_prof} -fcoverage-mapping #{sources.join(' ')} -o #{binary}`
# Collect profiling data
`./#{binary}`
# Convert raw data
`#{llvm_prof_data} merge -o #{prof_data} #{raw_prof}`
# Get export json file
`#{llvm_cov} export -Xdemangler=#{demangler} -instr-profile=#{prof_data} #{binary} > #{json}`

# Remove machine-dependent absolute paths
`#{sed} 's;/\([A-Za-z0-9_-]*/\)*tests/;./tests/;g' #{json} > #{json}.bak && mv #{json}.bak #{json}`