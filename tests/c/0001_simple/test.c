#include "header.h"

typedef struct TestStruct_
{
  int N;
  int val;
} TestStruct;

int ret_int(TestStruct const *tc)
{
  if ((tc->N > tc->val) && (tc->N >= 5))
  {
    return tc->N / 2;
  }
  else
  {
    return tc->N;
  }
}

char const *ret_str(TestStruct const *tc, int i0, int i1, int i2)
{
  if ((i0 > 10) && ((i1 == i2) || (i0 == i2)))
  {
    return "true";
  }
  else
  {
    return "false";
  }
}

int ret_int2(TestStruct const *tc)
{
  MACRO_IF(tc->val);
}

int main(int argc, char const *argv[])
{
  TestStruct tc1={0};
  TestStruct tc2={10};
  TestStruct tc3={1000};

  char const* fail0 = ret_str(&tc1, 10, 1, 1);
  char const* fail1 = ret_str(&tc1, 11, 1, 5);
  char const* sucess0 = ret_str(&tc1, 11, 1, 1);
  char const* sucess1 = ret_str(&tc1, 11, 1, 11);
  return ret_int2(&tc1) + ret_int(&tc1) + ret_int(&tc2);
}
