#!/bin/bash -e

export APT_KEY_DONT_WARN_ON_DANGEROUS_USAGE=1
wget -O - https://apt.llvm.org/llvm-snapshot.gpg.key | apt-key add -
echo "deb http://apt.llvm.org/buster/ llvm-toolchain-buster-11 main" >> /etc/apt/sources.list
echo "deb-src http://apt.llvm.org/buster/ llvm-toolchain-buster-11 main" >> /etc/apt/sources.list

DIR_NAME=$(dirname "${BASH_SOURCE[0]}")
cd $DIR_NAME
./install-package.sh libclang-11-dev libclang1-11 llvm-11
