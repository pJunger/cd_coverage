#!/bin/bash -e

function install_crate {
  $(cargo install $1) || $(cargo install-update $1)
}


# Make sure that cargo-update is there before install/updating crates.
install_crate cargo-update
for var in "$@"
do
    install_crate $var
done
