#!/bin/bash -e

# Adapted from https://doc.rust-lang.org/nightly/unstable-book/compiler-flags/source-based-code-coverage.html

prof_data=target/debug-cov/default.profdata
crate_name=$(cargo read-manifest | jq -r '.name')
target_dir=target/debug-cov
doctest_dir=$target_dir/doctestbins
# Clear potentially outdated profile data
rm -f $target_dir/*.profraw $prof_data

# Add instrumentation to binary
export RUSTFLAGS="$RUSTFLAGS -Zinstrument-coverage"

# Remap path prefix
# export abspath=`cargo metadata | jq -r '.workspace_root'`
# export RUSTFLAGS="$RUSTFLAGS --remap-path-prefix src=$abspath/src"

# Set rustdoc to instrument coverage & persist the binary 
export RUSTDOCFLAGS="-Zinstrument-coverage -Zunstable-options --persist-doctests ${doctest_dir}"

# Set up the pattern for the raw profile data
export LLVM_PROFILE_FILE="$target_dir/dlt-proto-%m.profraw"

# Run testsuite
cargo test --target-dir $target_dir -v

# Find all normal test binaries
test_objects=$(cargo test --no-run --message-format=json --target-dir $target_dir \
              | jq -r "select(.profile.test == true) | .filenames[]" \
              | grep -v dSYM -)

# Gets all test binaries including the one(s) from rustdoc & prefixes them with -object
object_command=$( \
      for file in \
        ${test_objects} ${doctest_dir}/*/rust_out; \
      do \
        [[ -x $file ]] && echo "-object $file "; \
      done \
    )
# Fun fact: The first positional argument is a binary as well. So we need to strip the '-object' once. Yay
object_command="${object_command/-object /}"

# Convert raw profile data.
prof_raw=$(find . -name "dlt-proto*.profraw")
cargo profdata -- merge -sparse ${prof_raw} -o $prof_data

# Generate HTML & the overview for CI purposes
fixed_options="-ignore-filename-regex=cargo/registry -Xdemangler=rustfilt -instr-profile=$prof_data $object_command src"

cargo cov -- show \
  -format=html \
  -output-dir=target/cov \
  -show-expansions -show-instantiations -show-line-counts-or-regions \
  $fixed_options

# This output will be parsed by GitLab.
cargo cov -- report \
  $fixed_options
