#!/bin/bash -e

mkdir -pv $APT_CACHE_DIR
apt-get -qq update
apt-get -qq -o dir::cache::archives="$APT_CACHE_DIR" install -y $@
