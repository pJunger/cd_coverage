// Copyright (c) 2018 cd_coverage developers
//
// Licensed under the Apache License, Version 2.0
// <LICENSE-APACHE or http://www.apache.org/licenses/LICENSE-2.0> or the MIT
// license <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. All files in the project carrying such notice may not be copied,
// modified, or distributed except according to those terms.

#[macro_use]
extern crate error_chain;
extern crate clap;
extern crate serde;
extern crate serde_json;
#[macro_use]
extern crate serde_derive;
extern crate validator;
#[macro_use]
extern crate validator_derive;
extern crate clang;
extern crate itertools;
extern crate num;
extern crate rust_decimal;
#[macro_use]
extern crate log;
#[macro_use]
extern crate lazy_static;

/// Contains all arguments from the command line
pub mod arguments;
/// Contains error definitions
pub mod error;

mod analyzer;
mod common;
mod input;
mod output;

use analyzer::cpp::ClangAnalyzer;
use analyzer::Analyzer;
use arguments::Arguments;
use input::json::LlvmCovExport;
use output::model::Model;

/// Run the analyzer and writes cd coverage to file
pub fn create_coverage_results(arguments: Arguments) -> error::Result<()> {
    info!("Analyzing arguments {:?}!", arguments.input);
    let json = input::get_json(arguments.input)?;

    let model = analyze(json)?;

    model.write_json(arguments.output)
}

/// Runs the analyzer and create the cd coverage results
pub fn analyze(json: LlvmCovExport) -> error::Result<Model> {
    info!("Creating model");
    // Todo: Put this demangler selection somewhere else
    let mut model = output::create_model(json)?;

    let analyzer = ClangAnalyzer::create_analyzer()?;
    analyzer.analyze_model(&mut model)?;

    Ok(model)
}
