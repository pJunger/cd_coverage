use clang::source::{SourceLocation, SourceRange};
use error;
use std::cmp::Ordering;

/// RegionKind as defined in CoverageMapping from llvm-cov
#[derive(Debug, Copy, Clone, Serialize, Hash, Ord, PartialOrd, Eq, PartialEq)]
pub enum RegionKind {
    /// A CodeRegion associates some code with a counter
    CodeRegion,

    /// An ExpansionRegion represents a file expansion region that associates
    /// a source range with the expansion of a virtual source file, such as
    /// for a macro instantiation or #include file.
    ExpansionRegion,

    /// A SkippedRegion represents a source range with code that was skipped
    /// by a preprocessor or similar means.
    SkippedRegion,

    /// A GapRegion is like a CodeRegion, but its count is only set as the
    /// line execution count when its the only region in the line.
    GapRegion,
}

impl RegionKind {
    pub fn from(num: u64) -> error::Result<RegionKind> {
        match num {
            0 => Ok(RegionKind::CodeRegion),
            1 => Ok(RegionKind::ExpansionRegion),
            2 => Ok(RegionKind::SkippedRegion),
            3 => Ok(RegionKind::GapRegion),
            x => bail!("Invalid Region: {}", x),
        }
    }
}

/// A code location
#[derive(Hash, Copy, Clone, Debug, Default, Serialize, Ord, PartialOrd, Eq, PartialEq)]
pub struct Location {
    /// Current line
    pub line: u32,
    /// Current column
    pub column: u32,
}

impl<'tu> From<SourceLocation<'tu>> for Location {
    fn from(loc: SourceLocation<'tu>) -> Self {
        Location::new(loc.get_file_location().line, loc.get_file_location().column)
    }
}

impl Location {
    pub fn new(line: u32, column: u32) -> Location {
        Location { line, column }
    }

    pub fn equal_to_src_location(&self, loc: SourceLocation) -> bool {
        (loc.get_file_location().line == self.line)
            && (loc.get_file_location().column == self.column)
    }
}

/// A span of source code, from start to end
#[derive(Hash, Copy, Clone, Debug, Default, Serialize, Eq, PartialEq)]
pub struct Span {
    /// The start of the span
    pub start: Location,
    /// The end of the span
    pub end: Location,
}

impl Span {
    pub fn new(start: Location, end: Location) -> Span {
        Span { start, end }
    }

    pub fn contains(&self, other: &Span) -> bool {
        (self.start <= other.start) && (self.end >= other.end)
    }

    /// Performs partial_cmp, but orders only if both spans are in a parent-child relationship
    pub fn cmp_inside(&self, other: &Span) -> Option<Ordering> {
        let start_end = self.start.cmp(&other.end);
        let end_start = self.end.cmp(&other.start);

        if start_end == Ordering::Greater || end_start == Ordering::Less {
            // There is no intersection, so we do not want to enforce any ordering
            None
        } else {
            let start_start = self.start.cmp(&other.start);
            let end_end = self.end.cmp(&other.end);
            match (start_start, end_end) {
                (Ordering::Equal, Ordering::Equal) => Some(Ordering::Equal),
                (x, y) if x == y => None, // No parent-child relationship -> No ordering
                (Ordering::Greater, _) | (_, Ordering::Less) => Some(Ordering::Less),
                _ => None,
            }
        }
    }
}

/// No total order! There are spans that engulf one another
impl PartialOrd for Span {
    fn partial_cmp(&self, other: &Span) -> Option<Ordering> {
        if self == other {
            Some(Ordering::Equal)
        } else if self.start > other.end {
            Some(Ordering::Greater)
        } else if self.end < other.start {
            Some(Ordering::Less)
        } else {
            // One is (partly) inside another
            None
        }
    }
}

impl<'tu> From<SourceRange<'tu>> for Span {
    fn from(range: SourceRange<'tu>) -> Self {
        Span::new(range.get_start().into(), range.get_end().into())
    }
}
