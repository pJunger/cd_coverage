use std::path::PathBuf;

use clap::{App, Arg};

use error;

/// All configuration parameters
pub struct Arguments {
    /// path to the llvm-cov export json file
    pub input: PathBuf,
    /// path to the output path
    pub output: PathBuf,
    /// Log filter as defined by env_logger
    pub log_filter: Option<String>,
}

impl Arguments {
    /// Parse command line arguments
    pub fn parse() -> error::Result<Arguments> {
        let matches = App::new(env!("CARGO_PKG_NAME"))
            .version(env!("CARGO_PKG_VERSION"))
            .author(env!("CARGO_PKG_AUTHORS"))
            .about("Gets Condition Decision Coverage from llvm-cov")
            .arg(
                Arg::with_name("json_input")
                    .short("i")
                    .long("input")
                    .value_name("JSON-FILE")
                    .help("The path to the llvm-cov export")
                    .takes_value(true)
                    .required(true),
            )
            .arg(
                Arg::with_name("output")
                    .short("o")
                    .long("output")
                    .value_name("PATH")
                    .help("Path to the result")
                    .takes_value(true)
                    .required(true),
            )
            .arg(
                Arg::with_name("log_filter")
                    .short("l")
                    .long("log_filter")
                    .value_name("FILTER")
                    .help("Log filter for env_logger")
                    .takes_value(true)
                    .default_value("info")
                    .required(false),
            )
            .get_matches();

        let input = matches
            .value_of("json_input")
            .ok_or("Json Input needed")
            .map(PathBuf::from)?;
        let output = matches
            .value_of("output")
            .ok_or("Output path needed")
            .map(PathBuf::from)?;

        let log_filter = matches.value_of("log_filter").map(|s| s.to_string());

        Ok(Arguments {
            input,
            output,
            log_filter,
        })
    }
}
