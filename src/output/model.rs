use serde::{Serialize, Serializer};
use std::collections::HashMap;
use std::path::PathBuf;

use clang::Entity;
use common::*;
use error;
use num::Zero;
use rust_decimal::Decimal;

#[derive(Default, Debug)]
pub struct Model(Vec<ConditionDecisionCoverage>);

impl Model {
    pub fn get_mut(&mut self) -> &mut Vec<ConditionDecisionCoverage> {
        &mut self.0
    }

    pub fn get_files(&self) -> Vec<PathBuf> {
        self.0
            .iter()
            .flat_map(|cdc| cdc.files.values())
            .map(|f| f.path.clone())
            .collect()
    }

    pub fn push(&mut self, cdc: ConditionDecisionCoverage) {
        self.0.push(cdc);
    }

    pub fn write_json(&self, path: PathBuf) -> error::Result<()> {
        info!("Writing to {:?}", path);
        let output = serde_json::to_string(&self.0)?;
        std::fs::write(path, output)?;
        Ok(())
    }
}

/// The root containing all coverage information
#[derive(Debug, Default, Serialize)]
pub struct ConditionDecisionCoverage {
    /// Contains a summary over all contained modules
    pub summary: CoverageResultSum,
    /// All contained modules
    #[serde(serialize_with = "serialize_files")]
    pub files: HashMap<PathBuf, Box<File>>,
}

/// Serialize files map as an array (of its values))
fn serialize_files<S>(map: &HashMap<PathBuf, Box<File>>, s: S) -> Result<S::Ok, S::Error>
where
    S: Serializer,
{
    map.values().collect::<Vec<_>>().serialize(s)
}

impl ConditionDecisionCoverage {
    pub fn files_mut(&mut self) -> &mut HashMap<PathBuf, Box<File>> {
        &mut self.files
    }

    /// Adds a constructed file into the Root node
    pub fn add_file(&mut self, file: File) -> error::Result<()> {
        debug!("Adding file: {:?}", file.path);
        if self
            .files
            .insert(file.path.clone(), Box::new(file))
            .is_none()
        {
            Ok(())
        } else {
            bail!("File was already added!")
        }
    }

    pub fn get_file_mut(&mut self, path: &PathBuf) -> error::Result<&mut File> {
        self.files
            .get_mut(path)
            .map(|m| &mut **m)
            .ok_or_else(|| error::ErrorKind::NoResultsForFile.into())
    }
}

/// An analyzed source File
#[derive(Debug, Serialize)]
pub struct File {
    /// Contains all results contained in this file
    pub summary: CoverageResultSum,
    /// The kind of Module
    pub path: PathBuf,
    /// All functions contained directly in this module
    pub functions: Vec<Function>,
}

impl File {
    pub fn new(path: PathBuf) -> Self {
        File {
            summary: CoverageResultSum::default(),
            path,
            functions: Vec::new(),
        }
    }

    pub fn add_function(&mut self, function: Function) {
        self.functions.push(function)
    }
}

#[derive(Debug, Serialize)]
pub struct FileLocation {
    pub translation_unit: PathBuf,
    pub definition_file: PathBuf,
}

impl FileLocation {
    pub fn in_src(file: PathBuf) -> Self {
        FileLocation {
            translation_unit: file.clone(),
            definition_file: file,
        }
    }

    pub fn in_hd(translation_unit: PathBuf, definition_file: PathBuf) -> Self {
        FileLocation {
            translation_unit,
            definition_file,
        }
    }
}

#[derive(Debug, Serialize, Clone)]
#[serde(untagged)]
pub enum FunctionName {
    Demangled(String),
    Mangled(String),
}

/// Represents all associated and free functions
#[derive(Debug, Serialize)]
pub struct Function {
    /// Name of function
    pub name: FunctionName,
    /// true if this function was called at all
    pub called: bool,
    /// The file in which this function is defined in
    #[serde(flatten)]
    pub file: FileLocation,
    /// The complete location of this function
    pub location: Span,
    /// All decisions that are located in this function
    pub expressions: Expressions,
}

impl Function {
    /// Create a new Function from a location
    pub fn new(name: String, file: FileLocation, location: Span) -> Function {
        Function {
            name: FunctionName::Mangled(name),
            called: false,
            file,
            location,
            expressions: Expressions::default(),
        }
    }

    pub fn get_demangled(&self) -> Option<String> {
        match self.name.clone() {
            FunctionName::Demangled(s) => Some(s),
            _ => None,
        }
    }

    pub fn get_mangled(&self) -> Option<String> {
        match self.name.clone() {
            FunctionName::Mangled(s) => Some(s),
            _ => None,
        }
    }

    pub fn set_demangled(&mut self, name: String) {
        self.name = FunctionName::Demangled(name);
    }

    pub fn set_called(&mut self) {
        self.called = true;
    }

    pub fn add_decision(&mut self, span: Span, hit_count: u64, was_true: bool, was_false: bool) {
        let e = Expression::from_decision(span, hit_count, was_true, was_false);
        self.expressions.add_expression(e);
    }

    /// Returns all expressions except for the first which is the whole function range
    pub fn get_expressions_mut(&mut self) -> &mut [Expression] {
        &mut self.expressions.0[1..]
    }

    pub fn get_expression(&self, entity: &Entity) -> Option<&Expression> {
        self.expressions.get().iter().find(|e| e.is_entity(entity))
    }

    pub fn get_expression_mut(&mut self, entity: &Entity) -> Option<&mut Expression> {
        self.expressions
            .get_mut()
            .iter_mut()
            .find(|e| e.is_entity(entity))
    }

    pub fn get_hit_count_of(&self, entity: &Entity) -> Option<u64> {
        self.get_expression(entity).map(|e| e.hit_count)
    }

    pub fn get_hit_count_of_contained_entity(&self, entity: &Entity) -> Option<u64> {
        let count = self
            .expressions
            .get()
            .iter()
            .filter(|e| e.contains_entity(entity))
            .max_by_key(|e| e.location.start)
            .map(|e| e.hit_count);
        count
    }
}

/// Summary of coverage results for a module/modules
#[derive(Debug, Copy, Clone, Serialize)]
pub struct CoverageResultSum {
    /// Covered conditions/decisions
    pub cd_covered: u64,
    /// Total conditions/decisions
    pub cd_total: u64,
    /// The relation between covered&uncovered conditions/decisions
    pub cd_percentage: Decimal,
    /// Covered functions
    pub functions_covered: u64,
    /// Total functions
    pub functions_total: u64,
    /// The relation between covered&uncovered functions
    pub functions_percentage: Decimal,
}

impl CoverageResultSum {
    /// Create Sum from function results
    pub fn from_func(covered: u64, total: u64, percentage: u64) -> Self {
        Self {
            functions_covered: covered,
            functions_total: total,
            functions_percentage: percentage.into(),
            ..Default::default()
        }
    }
}

impl Default for CoverageResultSum {
    /// Create an empty result
    fn default() -> CoverageResultSum {
        CoverageResultSum {
            cd_covered: 0,
            cd_total: 0,
            cd_percentage: Decimal::zero(),
            functions_covered: 0,
            functions_total: 0,
            functions_percentage: Decimal::zero(),
        }
    }
}

/// The type of condition - either && or ||
#[derive(Debug, Copy, Clone, Serialize, Hash, PartialOrd, Ord, PartialEq, Eq)]
pub enum ConditionType {
    And,
    Or,
}

/// The type of expression - either condition or decision
#[derive(Debug, Copy, Clone, Serialize, Hash, PartialOrd, Ord, PartialEq, Eq)]
pub enum ExpressionType {
    Unknown,
    Condition(ConditionType),
    Decision,
    Block,
}

#[derive(Debug, Copy, Clone, Serialize, Hash, PartialOrd, Eq, PartialEq)]
pub enum CoverageStatus {
    #[serde(rename = "xx")]
    NotCovered,
    #[serde(rename = "xf")]
    OnlyFalse,
    #[serde(rename = "tx")]
    OnlyTrue,
    #[serde(rename = "tf")]
    TrueFalse,
}

impl CoverageStatus {
    pub fn from_bool(t: bool, f: bool) -> CoverageStatus {
        match (t, f) {
            (false, false) => CoverageStatus::NotCovered,
            (true, false) => CoverageStatus::OnlyTrue,
            (false, true) => CoverageStatus::OnlyFalse,
            (true, true) => CoverageStatus::TrueFalse,
        }
    }
}

/// Any interesting expression, so only conditions & decisions
#[derive(Debug, Copy, Clone, Serialize, Hash, PartialOrd, Eq, PartialEq)]
pub struct Expression {
    /// The kind reported from llvm
    kind: RegionKind,
    /// The complete location of this expression
    pub location: Span,
    pub hit_count: u64,

    /// The kind of expression
    pub expression_type: ExpressionType,
    /// Coverage state for this expression
    pub coverage_status: CoverageStatus,
}

impl Expression {
    pub fn is_entity(&self, entity: &Entity) -> bool {
        entity
            .get_range()
            .filter(|r| self.location.start == r.get_start().into())
            .filter(|r| self.location.end == r.get_end().into())
            .is_some()
    }

    pub fn contains_entity(&self, entity: &Entity) -> bool {
        entity
            .get_range()
            .filter(|r| self.location.start <= r.get_start().into())
            .filter(|r| self.location.end >= r.get_end().into())
            .is_some()
    }

    /// Create an uncovered expression
    pub fn new(kind: RegionKind, location: Span, hit_count: u64) -> Expression {
        Expression {
            kind,
            location,
            hit_count,
            expression_type: ExpressionType::Unknown,
            coverage_status: CoverageStatus::NotCovered,
        }
    }

    pub fn from_decision(
        location: Span,
        hit_count: u64,
        was_true: bool,
        was_false: bool,
    ) -> Expression {
        Expression {
            kind: RegionKind::CodeRegion,
            location,
            hit_count,
            expression_type: ExpressionType::Decision,
            coverage_status: CoverageStatus::from_bool(was_true, was_false),
        }
    }

    pub fn set_true(&mut self) -> &Self {
        self.coverage_status = match self.coverage_status {
            CoverageStatus::OnlyTrue | CoverageStatus::NotCovered => CoverageStatus::OnlyTrue,
            _ => CoverageStatus::TrueFalse,
        };
        self
    }

    pub fn set_false(&mut self) -> &Self {
        self.coverage_status = match self.coverage_status {
            CoverageStatus::OnlyFalse | CoverageStatus::NotCovered => CoverageStatus::OnlyFalse,
            _ => CoverageStatus::TrueFalse,
        };
        self
    }
}

#[derive(Default, Serialize, Debug)]
pub struct Expressions(Vec<Expression>);

impl Expressions {
    pub fn get(&self) -> &Vec<Expression> {
        &self.0
    }

    pub fn get_mut(&mut self) -> &mut Vec<Expression> {
        &mut self.0
    }

    pub fn from_expressions(expressions: Vec<Expression>) -> Self {
        Expressions(expressions)
    }

    pub fn add_expressions(&mut self, expressions: Vec<Expression>) {
        for expression in expressions {
            self.add_expression(expression);
        }
    }

    pub fn add_expression(&mut self, expr: Expression) {
        self.0.push(expr);
    }
}
