pub mod model;

use itertools::*;

use self::model::*;
use error;
use input::json;

pub fn create_model(json: json::LlvmCovExport) -> error::Result<Model> {
    let mut results = Model::default();
    for export_object in json.data {
        debug!(
            "Adding export object: CoverageMapping({:?})",
            export_object
                .files
                .iter()
                .map(|f| &f.file_path)
                .collect_vec()
        );
        let mut root = ConditionDecisionCoverage::default();
        // Get all files & the total results
        for file in export_object.files {
            debug!("Processing file {:?}", file.file_path);
            let mut module = File::new(file.file_path);
            let fun = {
                let fun_result = file.summary.functions;
                CoverageResultSum::from_func(
                    fun_result.covered,
                    fun_result.total(),
                    fun_result.percent,
                )
            };
            module.summary = fun;

            if let Err(e) = root.add_file(module) {
                error!("Error occurred while adding a file to the result: {}", e);
            }
        }

        // Find single functions and get their data & associate with the files
        for raw_func in export_object.functions {
            //            let demangled = raw_func.demangled(demangler)?;

            for file in &raw_func.file_paths {
                // Todo: Differentiate between files in src & header
                let file_location = FileLocation::in_src(file.to_path_buf());
                let mut func =
                    Function::new(raw_func.name.to_string(), file_location, raw_func.span());
                if raw_func.called() {
                    func.set_called();
                }
                func.expressions = raw_func.expressions();
                debug!("Adding function to module: {:?}", func);
                // Todo: Add function in all files it references

                let path = raw_func.file_path()?;
                let file = root.get_file_mut(&path)?;
                file.add_function(func);
            }
        }
        results.push(root);
    }

    trace!("The model is: {:?}", results);
    Ok(results)
}
