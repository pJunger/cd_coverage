/*===----------------------------------------------------------------------===//
//
// The json code coverage export follows the following format
// Root: dict => Root Element containing metadata
// -- Data: array => Homogeneous array of one or more export objects
// ---- Export: dict => Json representation of one CoverageMapping
// ------ Files: array => List of objects describing coverage for files
// -------- File: dict => Coverage for a single file
// ---------- Segments: array => List of Segments contained in the file
// ------------ Segment: dict => Describes a segment of the file with a counter
// ---------- Expansions: array => List of expansion records
// ------------ Expansion: dict => Object that descibes a single expansion
// -------------- CountedRegion: dict => The region to be expanded
// -------------- TargetRegions: array => List of Regions in the expansion
// ---------------- CountedRegion: dict => Single Region in the expansion
// ---------- Summary: dict => Object summarizing the coverage for this file
// ------------ LineCoverage: dict => Object summarizing line coverage
// ------------ FunctionCoverage: dict => Object summarizing function coverage
// ------------ RegionCoverage: dict => Object summarizing region coverage
// ------ Functions: array => List of objects describing coverage for functions
// -------- Function: dict => Coverage info for a single function
// ---------- Filenames: array => List of filenames that the function relates to
// ---- Summary: dict => Object summarizing the coverage for the entire binary
// ------ LineCoverage: dict => Object summarizing line coverage
// ------ FunctionCoverage: dict => Object summarizing function coverage
// ------ InstantiationCoverage: dict => Object summarizing inst. coverage
// ------ RegionCoverage: dict => Object summarizing region coverage
//
//===----------------------------------------------------------------------===*/

use std::fs::File;
use std::path::{Path, PathBuf};

use serde_json;
use validator::{Validate, ValidationError};

use common::*;
use error;
use output::model;

/// Custom validation rule that can validate members that are 'Validate' themselves
fn validate(container: &(impl Validate + std::fmt::Debug)) -> Result<(), ValidationError> {
    container.validate().map_err(|err| {
        error!("Validation error: {}", err);

        ValidationError::new("Container '{}' is invalid")
    })
}

fn validate_vec(mappings: &[impl Validate + std::fmt::Debug]) -> Result<(), ValidationError> {
    mappings.iter().try_for_each(validate)
}

/// Root Element containing metadata
#[derive(Debug, Serialize, Deserialize, Validate)]
pub struct LlvmCovExport {
    /// Version of the llvm-cov export json file
    #[validate(contains = "2.0.0")]
    version: String,
    /// The type of JSON file
    #[serde(rename = "type")]
    #[validate(contains = "llvm.coverage.json.export")]
    json_type: String,
    /// Homogeneous array of one or more export objects
    #[validate(custom = "validate_vec")]
    pub data: Vec<CoverageMapping>,
}

impl LlvmCovExport {
    /// Read in the passed llvm-cov export result into memory
    pub fn new(path: &Path) -> error::Result<LlvmCovExport> {
        debug!("Opening the file in read-only mode.");
        let file = File::open(path)?;

        debug!("Read the JSON contents of the file.");
        let root: LlvmCovExport = serde_json::from_reader(file)?;
        debug!("Checking for correct JSON schema");
        root.validate()?;

        trace!("Read in model is {:?}", root);
        Ok(root)
    }

    /// Checks that the JSON file is of the expected schema
    /// Todo: Remove assert and return Results
    #[allow(dead_code)]
    fn check_type(&self) -> error::Result<()> {
        let json_type = "llvm.coverage.json.export";
        ensure!(
            self.json_type == json_type,
            "Type of JSON file must be {}",
            json_type
        );
        Ok(())
    }

    /// Checks that the JSON file is of the expected version
    /// Todo: Remove assert and return Results
    #[allow(dead_code)]
    fn check_version(&self) -> error::Result<()> {
        let version = "2.0.0";
        ensure!(
            self.version == version,
            "Currently only version {} is supported",
            version
        );
        Ok(())
    }
}

/// Json representation of one CoverageMapping
#[derive(Debug, Serialize, Deserialize, Validate)]
pub struct CoverageMapping {
    /// List of objects describing coverage for files
    pub files: Vec<FileResult>,
    /// List of objects describing coverage for functions
    #[validate(custom = "validate_vec")]
    pub functions: Vec<Function>,
    /// Object summarizing the coverage for the entire binary
    #[serde(rename = "totals")]
    pub summary: Summary,
}

/// Coverage for a single file
#[derive(Debug, Serialize, Deserialize)]
pub struct FileResult {
    /// Path to the current file
    #[serde(rename = "filename")]
    pub file_path: PathBuf,
    /// List of Segments contained in the file
    pub segments: Vec<Segment>,
    /// List of expansion records
    expansions: Vec<Expansion>,
    /// Object summarizing the coverage for the entire binary
    pub summary: Summary,
}

/// Describes a segment of the file with a counter
#[derive(Debug, Serialize, Deserialize)]
pub struct Segment(u64, u64, u64, u64, u64);

/// Object that describes a single expansion
/// Exists only for Preprocessor Macros(?)
#[derive(Debug, Serialize, Deserialize)]
pub struct Expansion {
    /// The region to be expanded
    source_region: Region,
    /// List of Regions in the expansion
    target_regions: Vec<Region>,
    /// Files which contain the expansions?
    #[serde(rename = "filenames")]
    file_paths: Vec<PathBuf>,
}

/// Coverage info for a single function
#[derive(Debug, Serialize, Deserialize, Validate)]
pub struct Function {
    /// Function name
    /// (in C++ complete with namespace etc.)
    pub name: String,
    /// Call count
    pub count: u64,
    /// List of Regions in the function
    /// The first region is always the complete function block
    pub regions: Vec<Region>,
    /// List of filenames that the function relates to
    /// It seems that the first entry is the file which contains the function
    /// Following entries seem to be used for the definition of macro expansions
    #[serde(rename = "filenames")]
    #[validate(length(min = 1))]
    pub file_paths: Vec<PathBuf>,
}

impl Function {
    /// Get the path in which this function is defined in
    pub fn file_path(&self) -> error::Result<PathBuf> {
        if let Some(path) = self.file_paths.first() {
            Ok(path.to_path_buf())
        } else {
            bail!("Function seems to be related to no file!");
        }
    }

    /// Get files which contain expansions
    pub fn expansion_files(&self) -> Vec<PathBuf> {
        self.file_paths
            .iter()
            .skip(1)
            .map(|p| p.to_path_buf())
            .collect()
    }

    pub fn called(&self) -> bool {
        self.count > 0
    }

    pub fn span(&self) -> Span {
        self.regions[0].span()
    }

    /// Gets all contained expressions in this function
    pub fn expressions(&self) -> model::Expressions {
        let vec = self.regions.iter().map(|r| r.to_expression()).collect();
        model::Expressions::from_expressions(vec)
    }
}

/// A single region
/// Can be a block, decision, condition etc.
#[derive(Debug, Serialize, Deserialize)]
pub struct Region(u32, u32, u32, u32, u64, u64, u64, u64);

impl Region {
    /// Gets the start of the region
    fn start(&self) -> Location {
        Location::new(self.0, self.1)
    }

    /// Gets the end of the region
    fn end(&self) -> Location {
        Location::new(self.2, self.3)
    }

    fn span(&self) -> Span {
        Span::new(self.start(), self.end())
    }

    /// Get the number of times this region has been hit
    fn call_count(&self) -> u64 {
        self.4
    }

    //    /// Get the file id for this region
    //    fn file_id(&self) -> u64 {
    //        self.5
    //    }
    //
    //    /// Get the expanded file id for this region
    //    fn expanded_file_id(&self) -> u64 {
    //        self.6
    //    }

    /// Get the kind of region
    fn kind(&self) -> RegionKind {
        RegionKind::from(self.7).expect("Invalid JSON file!")
    }

    /// Creates the initial expression
    fn to_expression(&self) -> model::Expression {
        model::Expression::new(self.kind(), self.span(), self.call_count())
    }
}

/// Object summarizing the coverage
#[derive(Debug, Serialize, Deserialize)]
pub struct Summary {
    /// Coverage result for lines
    lines: CoverageResult,
    /// Coverage result for functions
    pub functions: CoverageResult,
    /// Coverage result for template(?) instantiations
    pub instantiations: CoverageResult,
    /// Coverage result for regions
    regions: CoverageResult,
}

/// Single Coverage Result
// Todo: Find out what these numbers mean exactly - is count == total?
#[derive(Debug, Serialize, Deserialize)]
pub struct CoverageResult {
    count: u64,
    pub covered: u64,
    #[serde(rename = "notcovered")]
    pub not_covered: Option<u64>,
    pub percent: u64,
}

impl CoverageResult {
    /// Calculate the total result
    pub fn total(&self) -> u64 {
        if let Some(not_covered) = self.not_covered {
            self.covered + not_covered
        } else {
            self.covered
        }
    }
}
