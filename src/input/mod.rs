use std::path::Path;

pub mod json;

use error;

pub fn get_json<P: AsRef<Path>>(path: P) -> error::Result<json::LlvmCovExport> {
    json::LlvmCovExport::new(&path.as_ref())
}
