pub mod cpp;

use error;
use output::model::*;

/// Trait that may be used to abstract away C++ / Rust / ... analyzers
pub trait Analyzer: Sized {
    /// Instantiate analyzer for the passed files
    fn create_analyzer() -> error::Result<Self>;
    fn analyze_function(&self, function: &mut Function) -> error::Result<()>;

    /// Analyze a single file
    /// Fills the passed module with gathered information
    fn analyze_file(&self, file: &mut File) -> error::Result<()> {
        for function in &mut file.functions {
            self.analyze_function(function).unwrap()
        }
        Ok(())
    }

    fn analyze_model(&self, model: &mut Model) -> error::Result<()> {
        for cov in model.get_mut() {
            for module in cov.files_mut().values_mut() {
                self.analyze_file(module)?;
            }
        }
        Ok(())
    }
}
