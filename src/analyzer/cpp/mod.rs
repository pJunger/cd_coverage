use clang::source::*;
use clang::*;
use itertools::Itertools;

use analyzer::Analyzer;
use error;
use output::model;

// Analysis steps:
// - if/else if/else/for/while branch:
//     - check if block below has count != 0
// - if without else / for/while without iteration:
//     - count if conditional branch has different count than scope below
// - && conditions:
//     - For true: candidate (rhs) count != 0
//     - For false: candidate < lhs
// - || conditions:
//     - For true: candidate < lhs
//     - For false: either next condition != 0 or decision was false

// If used as an executable, this should not be a problem
lazy_static! {
    static ref CLANG: Clang = Clang::new().expect("Could not initialize Clang analyzer");
}

pub struct ClangAnalyzer {
    index: Index<'static>,
}

impl ClangAnalyzer {
    /// Create new Index & start analyzer
    pub fn new() -> ClangAnalyzer {
        ClangAnalyzer {
            index: Index::new(&CLANG, false, false),
        }
    }

    fn func_from_file<'tu>(
        &self,
        func: &model::Function,
        file: &'tu File,
    ) -> error::Result<Entity<'tu>> {
        let start = &func.location.start;
        let func_start = file.get_location(start.line as u32, start.column as u32);

        let func_block_entity = func_start
            .get_entity()
            .ok_or(format!("Nothing found at this location {:?}", func_start))?;
        let func_entity = func_block_entity
            .get_semantic_parent()
            .ok_or("Block has no parent?")?;
        Ok(func_entity)
    }

    fn expression_start_from_file<'tu>(
        &self,
        expression: &model::Expression,
        file: &File<'tu>,
    ) -> error::Result<Entity<'tu>> {
        let start = &expression.location.start;

        let expr_start = file.get_location(start.line as u32, start.column as u32);
        let previous_loc = expr_start.get_file_location().offset;
        let entity = file
            .get_offset_location(previous_loc)
            .get_entity()
            .ok_or("No expression found at location!")?;
        Ok(entity)
    }

    fn analyze_function(&self, func: &mut model::Function, file: &File) -> error::Result<()> {
        let entity = self.func_from_file(func, &file)?;
        if !entity.is_definition() {
            bail!(
                "Entity is no function definition: {} at {:?}",
                entity.get_token_string(),
                entity.get_location()
            );
        } else if !entity.is_function() {
            bail!(
                "Entity is no function: {} at {:?}",
                entity.get_token_string(),
                entity.get_location()
            );
        } else if func.get_mangled() != entity.get_mangled_name() {
            bail!(
                "Function has incorrect name!: {} at {:?}",
                entity.get_token_string(),
                entity.get_location()
            );
        }

        let display_name = entity.get_display_name().ok_or(format!(
            "Cannot find demangled name for {}?",
            func.get_mangled().unwrap()
        ))?;

        info!("Analyzing function {:?}", display_name);
        func.set_demangled(display_name);

        self.visit_func(func, entity)?;
        Ok(())
    }

    fn log_children(children: &[Entity]) {
        if !children.is_empty() {
            debug!(
                "The children are [{}]{{\n    {}\n}}",
                children.len(),
                children
                    .iter()
                    .map(|c| format!(
                        "kind: \"{:?}\", tokens: {}",
                        c.get_kind(),
                        c.get_token_string() // get_display_name().unwrap_or("None".to_string())
                    ))
                    .join(",\n    ")
            );
        } else {
            trace!("No children");
        }
    }

    /// Todo: This will become even more complex: For do-while  loops we must check for the count of any blocks that contain a break or return
    /// For this to work, we need to have access to the loop block
    fn visit_entity(
        &self,
        func: &mut model::Function,
        entity: &Entity,
        parent: Option<&Entity>,
    ) -> error::Result<()> {
        trace!("Visiting {:?}", entity.get_token_string());
        let children = entity.get_children();

        for (idx, child) in children.iter().enumerate() {
            ClangAnalyzer::log_children(&child.get_children());

            if child.get_kind() == EntityKind::DoStmt {
                ClangAnalyzer::analyze_do_while(func, entity, parent, &children, idx, child)?;
            } else if child.get_kind() == EntityKind::ForStmt {
                ClangAnalyzer::analyze_for(func, child)?;
            } else if child.get_kind() == EntityKind::SwitchStmt {
                ClangAnalyzer::analyze_switch(func, child)?;
            } else if child.get_kind() == EntityKind::ConditionalOperator {
                ClangAnalyzer::analyze_conditional_operator(func, child)?;
            } else if child.is_decision() {
                ClangAnalyzer::analyze_decision_fallback(func, child)?;
            } else if let Some(condition_type) = child.get_condition_type() {
                ClangAnalyzer::analyze_condition(func, child, condition_type)?;
            }
            self.visit_entity(func, child, Some(entity))?;
        }
        Ok(())
    }

    fn analyze_condition(
        func: &mut model::Function,
        child: &Entity,
        condition_type: model::ConditionType,
    ) -> error::Result<()> {
        debug!("Analyzing Condition {}\n", child.get_token_string());
        let expr: &mut model::Expression = child
            .get_child(0)
            .as_ref()
            .and_then(|x| func.get_expression_mut(x))
            .ok_or("No corresponding expression found for condition")?;
        expr.expression_type = model::ExpressionType::Condition(condition_type);

        Ok(())
    }

    fn analyze_decision_fallback(func: &mut model::Function, child: &Entity) -> error::Result<()> {
        debug!("Analyzing Decision: {}\n", child.get_token_string());
        // Now we need to count how many times the decision was true
        let true_count = child
            .get_child(1)
            .as_ref()
            .and_then(|x| func.get_hit_count_of(x))
            .ok_or("No corresponding expression found for statement after decision")?;
        let expr = child
            .get_child(0)
            .as_ref()
            .and_then(|x| func.get_expression_mut(x))
            .ok_or("No corresponding expression found for decision")?;
        match (expr.hit_count, true_count) {
            (0, _) => {}
            (_, 0) => {
                expr.set_false();
            }
            (x, y) if x == y => {
                expr.set_true();
            }
            _ => {
                expr.set_true();
                expr.set_false();
            }
        }
        expr.expression_type = model::ExpressionType::Decision;

        Ok(())
    }

    fn analyze_conditional_operator(
        func: &mut model::Function,
        child: &Entity,
    ) -> error::Result<()> {
        debug!("Analyzing Decision (?:): {}\n", child.get_token_string());

        // Now we need to count how many times the decision was true
        let true_count = child
            .get_child(1)
            .as_ref()
            .and_then(|x| func.get_hit_count_of(x))
            .ok_or("No true expression found")?;
        let false_count = child
            .get_child(2)
            .as_ref()
            .and_then(|x| func.get_hit_count_of(x))
            .ok_or("No false expression found")?;

        let range = child
            .get_child(0)
            .and_then(|condition| condition.get_range())
            .ok_or("No location for condition?")?;

        // Add this for the condition
        func.add_decision(
            range.into(),
            true_count + false_count,
            true_count > 0,
            false_count > 0,
        );

        Ok(())
    }

    fn analyze_switch(func: &mut model::Function, switch: &Entity) -> error::Result<()> {
        debug!(
            "Analyzing Decision (switch): {}\n",
            switch.get_token_string()
        );
        // Now we need to count how many times the switch was hit
        let total_count = func
            .get_hit_count_of_contained_entity(switch)
            .ok_or("No corresponding expression found for switch")?;

        // We now need to analyze every case/default branch
        for child in switch.get_child(1).unwrap().get_children().iter() {
            debug!("Child in switch is: {}", child.get_token_string());
            let expr = func
                .get_expression_mut(child)
                .ok_or("No corresponding expression found for ?:")?; // Does not work
            let true_count = expr.hit_count;

            if total_count != true_count {
                expr.set_false();
            }
            if true_count > 0 {
                expr.set_true();
            }

            expr.expression_type = model::ExpressionType::Decision;
        }

        Ok(())
    }

    fn analyze_do_while(
        func: &mut model::Function,
        entity: &Entity,
        parent: Option<&Entity>,
        children: &[Entity],
        idx: usize,
        child: &Entity,
    ) -> error::Result<()> {
        panic!("Cannot reasonably analyze do-while at the moment!");
        debug!(
            "Analyzing Decision (do-while): {}\n",
            child.get_token_string()
        );
        debug!("Next? {:?}", children.get(idx + 1));
        let parent = parent.expect("Do-while cannot be top level?");
        // Do While loops will be executed once even if the condition is false ->
        let cur_idx = parent
            .get_children()
            .iter()
            .find_position(|x| *x == entity)
            .map(|x| x.0)
            .expect("Could not find index for current entity!");
        let post_count = parent
            .get_child(cur_idx + 1)
            .as_ref()
            .and_then(|x| func.get_hit_count_of(x))
            .ok_or("No corresponding expression found for statement after do-while")?;
        let pre_count = child
            .get_child(0)
            .as_ref()
            .and_then(|x| func.get_hit_count_of(x))
            .ok_or("No corresponding expression found for loop body in do-while")?;
        let expr = child
            .get_child(1)
            .as_ref()
            .and_then(|x| func.get_expression_mut(x))
            .ok_or("No corresponding expression found for do-while")?;

        // while never hit -> xx
        // post hit -> xf
        // statement hit more than once tx
        if expr.hit_count == 0 {
        } else {
            if post_count > 0 {
                expr.set_false();
            }

            if pre_count > 1 {
                expr.set_true();
            }
        }
        expr.expression_type = model::ExpressionType::Decision;
        Ok(())
    }

    fn analyze_for(func: &mut model::Function, child: &Entity) -> error::Result<()> {
        debug!("Analyzing Decision (for): {}\n", child.get_token_string());

        trace!(
            "Loop body: {:?}",
            child.get_child(3).unwrap().get_token_string()
        );
        // Now we need to count how many times the decision was true
        let true_count = child
            .get_child(3) // Skip the three statements in for
            .as_ref()
            .and_then(|x| func.get_hit_count_of(x))
            .ok_or("No corresponding expression found for statement after decision")?;
        debug!("Child is here: {:?}", child);

        let expr = child
            .get_child(1)
            .as_ref()
            .and_then(|x| func.get_expression_mut(x))
            .ok_or("No corresponding expression found for decision")?;

        match (expr.hit_count, true_count) {
            (0, _) => {}
            (_, 0) => {
                expr.set_false();
            }
            (x, y) if x == y => {
                expr.set_true();
            }
            _ => {
                expr.set_true();
                expr.set_false();
            }
        }

        expr.expression_type = model::ExpressionType::Decision;

        Ok(())
    }

    fn visit_func(&self, func: &mut model::Function, entity: Entity) -> error::Result<()> {
        self.visit_entity(func, &entity, None)
    }

    fn analyze_region(&self, expression: &mut model::Expression, file: &File) -> error::Result<()> {
        let entity = self.expression_start_from_file(expression, file)?;

        debug!(
            "Expression at {:?} of type {:?} is decision?: {:?}",
            expression.location,
            entity.get_kind(),
            ExpressionEntity::new(entity, file)
        );
        Ok(())
    }
}

#[derive(Debug)]
enum ExpressionKind {
    Decision,
    Condition,
    Unknown,
}

#[derive(Debug)]
struct ExpressionEntity<'a> {
    entity: Entity<'a>,
    kind: ExpressionKind,
}

impl<'a, 'b> ExpressionEntity<'a> {
    fn new(entity: Entity<'a>, file: &File<'b>) -> Self {
        ExpressionEntity {
            entity,
            kind: ExpressionKind::Unknown,
        }
    }
}

trait KindAnalyzer {
    fn is_function(&self) -> bool;
    fn is_decision(&self) -> bool;
    fn get_condition_type(&self) -> Option<model::ConditionType>;
    fn get_token_string(&self) -> String;
}

impl<'a> KindAnalyzer for Entity<'a> {
    fn is_function(&self) -> bool {
        matches!(
            self.get_kind(),
            EntityKind::FunctionDecl
                | EntityKind::FunctionTemplate
                | EntityKind::Method
                | EntityKind::Constructor
                | EntityKind::ConversionFunction
        )
    }

    fn is_decision(&self) -> bool {
        matches!(
            self.get_kind(),
            EntityKind::ConditionalOperator
                | EntityKind::IfStmt
                | EntityKind::SwitchStmt
                | EntityKind::WhileStmt
        )
    }

    /// Gets the symbol of a binary operator as libclang does not expose this
    /// Idea: We can get the arguments & tokens of entities -> Get the first token after the
    /// first child's
    fn get_condition_type(&self) -> Option<model::ConditionType> {
        if let Some(child) = self.get_child(0) {
            let num_prefix = child.get_range().iter().map(|r| r.tokenize().len()).sum();

            self.get_range()
                .iter()
                .flat_map(|r| r.tokenize())
                .map(|t| t.get_spelling())
                .nth(num_prefix)
                .and_then(|s| match s.as_str() {
                    "&&" => Some(model::ConditionType::And),
                    "||" => Some(model::ConditionType::Or),
                    _ => None,
                })
        } else {
            None
        }
    }

    fn get_token_string(&self) -> String {
        self.get_range()
            .iter()
            .flat_map(|r| r.tokenize())
            .map(|t| t.get_spelling())
            .join(" ")
    }
}

impl Analyzer for ClangAnalyzer {
    /// Get a analyzer for C/C++ code
    fn create_analyzer() -> error::Result<Self> {
        Ok(Self::new())
    }

    fn analyze_function(&self, function: &mut model::Function) -> error::Result<()> {
        let file = &function.file;
        trace!("Path to translation unit: {:#?}", file.translation_unit);
        let tu = self.index.parser(&file.translation_unit).parse()?;
        let def_file = tu
            .get_file(&file.definition_file)
            .ok_or("Incorrect definition file!")?;

        self.analyze_function(function, &def_file)
    }

    //    fn analyze_file(&self, file: &mut model::File) -> error::Result<()> {
    //        let file = &function.file;
    //        trace!("Path to translation unit: {:#?}", file.translation_unit);
    //        let tu = self.index.parser(&file.translation_unit).parse()?;
    //        let def_file = tu
    //            .get_file(&file.definition_file)
    //            .ok_or("Incorrect definition file!")?;
    //        tu.get_entity().visit_children(|entity, parent| {
    //            if entity.get_kind() == EntityKind::IfStmt {
    //                // Parent contains variables
    //            }
    //        })
    //    }
}
