// Copyright (c) 2017 cd_coverage developers
//
// Licensed under the Apache License, Version 2.0
// <LICENSE-APACHE or http://www.apache.org/licenses/LICENSE-2.0> or the MIT
// license <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. All files in the project carrying such notice may not be copied,
// modified, or distributed except according to those terms.

//! `cd_coverage` errors
use clang;
use serde_json;
use validator;

// Creates Result type, conversions from external errors
error_chain! {
    foreign_links {
        Io(::std::io::Error);
        Serde(serde_json::Error);
        ClangSource(clang::SourceError);
        JsonError(validator::ValidationErrors);
    }

    errors {
        NoResultsForFile {
                description("File not found in LLVM results")
                display("File not found in LLVM results")
        }
    }
}
